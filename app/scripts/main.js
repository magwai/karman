$(document).ready(function () {
	var $doc = $(document);
	// mask for input tel
	$(".js-tel").mask("+7 (999) 999-99-99", { placeholder: "+7 (___) ___-__-__" });

	wrapToScale('.scaling');
	// see also carousel
	$('.catalog-list-slider').royalSlider({
		loop: 					false,
		controlNavigation:		'none',
		arrowsNavAutoHide: 		false,
	});
	$('.styled-select').jqTransSelect();

	$('.js-scroll').jScrollPane();
	var myTimeout = null;
	$(window).resize(function(){
		clearTimeout(myTimeout);
		myTimeout = setTimeout(function(){
			$('.js-scroll').jScrollPane();
		}, 100);
	});

	$(".popup").fancybox({
		padding: 0,
		fitToView: false,
		autoSize: true,
		openEffect: 'none',
		closeEffect: 'none',
		beforeShow: function(e) {
			$('.styled-select').jqTransSelect();
		}
	});

	$doc.on('click', '.cart-btn', function () {
		var $t = $(this);
		$t
		.closest('.cart-widget')
		.toggleClass('active')
		;
	});

	// close on focus lost
	$doc.click(function(e) {
		var $trg = $(e.target);
		if (!$trg.closest(".cart-widget").length) {
			$('.cart-widget').removeClass('active');
		}
	});

	// counter plus / minus
	$doc.on('click', '.js-plus', function(e){
		var
		fieldName = $(this).attr('field')
		, $input = $('input[name='+fieldName+']')
		, max = $input.attr('data-max')
		, currentVal = parseInt($input.val())
		;
		e.preventDefault();
		if (!isNaN(currentVal)) {
			$input.val(currentVal + 1);
		} else {
			$input.val(1);
		}
		if ( parseInt( $input.val() ) > max) {
			$input.val(max);
		}
	});
	$doc.on('click', '.js-minus', function(e){
		var
		fieldName = $(this).attr('field')
		, $input = $('input[name='+fieldName+']')
		, min = $input.attr('data-min')
		, currentVal = parseInt($input.val())
		;
		e.preventDefault();
		if (!isNaN(currentVal) && currentVal > min) {
			$input.val(currentVal - 1);
		} else {
			$input.val(min);
		}
	});
	$doc.on('keydown', '.js-num', function(e){
		if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 || (e.keyCode == 65 && e.ctrlKey === true) || (e.keyCode == 67 && e.ctrlKey === true) || (e.keyCode == 88 && e.ctrlKey === true) || (e.keyCode >= 35 && e.keyCode <= 39)) {
			return;
		}
		if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
			e.preventDefault();
		}
	});
	$doc.on('keyup', '.js-num', function(e){
		var
		$t = $(this)
		, max = $t.attr('data-max')
		, min = $t.attr('data-min')
		, currentVal = $t.val()*1
		;
		function isInteger(num) {
			return (num ^ 0) === num;
		}
		if (!isInteger(currentVal) ) {
			$t.val(min);
		}
		if ( currentVal <= (min - 1)) {
			$t.val(min)
		}
		if ( currentVal > max) {
			$t.val(max);
		}
	});
	// .counter plus / minus
	$doc.on('mouseenter', '.checkbox-radio img, .color-select img', function(){
		var
		$t = $(this)
		, $tooltip = $('<div class="checkbox-tooltip"></div>')
		;
		$tooltip.text($t.attr('alt'));
		$t.after($tooltip);
	});
	$doc.on('mouseleave', '.checkbox-radio img, .color-select img', function(){
		$(this).next('.checkbox-tooltip').remove();
	});

	// ------------------------------account controls for inputs
	var adrField = null;
	$('.controlled').attr('disabled', true);
	// edit control-field
	$doc.on('click', '.edit-form', function(e){
		var
		$trg= $(e.target)
		, $field = $trg.closest('.control-field')
		, switchControls = function(){
			$trg.parent().find('a').toggleClass('invisible');
		}
		, switchDisabled = function(){
			$field.find('.controlled').toggleDisabled();
		}
		, renameAddresses = function(){
			$('.acc-addr').find('.control-field').each(function(i, obj){
				$(this)
				.find('label').text('Адрес '+(i+1)).attr('for', 'addrText'+(i+1))
				.end()
				.find('textarea').attr('id', 'addrText'+(i+1))
				;
			});
		}
		;
		if (adrField == null) {
			adrField = $('.acc-addr').find('.control-field')[0].outerHTML;
		}
		if ($trg.hasClass('edit-form__edit')) {
			switchDisabled();
			switchControls();
		}
		if ($trg.hasClass('edit-form__save')) {
			switchDisabled();
			switchControls();
		}
		if ($trg.hasClass('edit-form__del')) {
			$field.remove();
			renameAddresses();
		}
	});
	// add address
	$doc.on('click', '.acc-addr__btn-add', function(){
		if (adrField == null) {
			adrField = $('.acc-addr').find('.control-field')[0].outerHTML;
		}
		var
		$parent = $(this).parent()
		, maxCount = $parent.attr('data-max-addreses')
		, fieldsCount = $parent.find('.control-field').length
		, $clone = $(adrField)
		, clearFields = function(){
			$clone
			.find('label').text('Адрес '+(fieldsCount+1)).attr('for', 'addrText'+(fieldsCount+1))
			.end()
			.find('textarea').attr('id', 'addrText'+(fieldsCount+1)).text('')
			;
		}
		;
		clearFields();
		if ( fieldsCount >= maxCount ) {
			// alert('Можно добавить не более '+maxCount+' адресов.');
			$.fancybox.open([
			{
				href : '#no-more-addr'
			}
			], {
				fitToView: false,
				openEffect: 'none',
				padding : 0
			});
			return
		} else {
			$parent.append($clone);
		}
	});
	// ------------------------------end - account controls for inputs
	$doc.on('click', '.js-order-trigger', function () {
		$(this)
		.toggleClass('active')
		.closest('.js-order')
		.toggleClass('active')
		.find('.js-order-items')
		.slideToggle(300)
		;
		setTimeout(function() {
			imgScale( $('.js-order-items').find('img') )
		}, 10);
	});
});

function imgScale(selector) {
	$(selector).imageScale({
		rescaleOnResize: true
	});
}

function wrapToScale(className){
	var
	$items = $(className)
	, count = $items.length
	;
	$items.each(function(){
		$(this)
		.wrap('<div class="img-scaler"></div>')
		.wrap('<div class="img-scaler__wrap"></div>')
		;
		if (!--count) imgScale(".img-scaler img");
	});
};
$.fn.toggleDisabled = function() {
	return this.each(function() {
		this.disabled = !this.disabled;
	});
};